import Header from '../Components/Header';
import Head from 'next/head';

const about = () => {
    return (
        <div>
            <Head>
                <title>Privacy Page</title>
            </Head>
            <Header />
            <h1>This is our About us page</h1>
        </div>
    )
}

export default about