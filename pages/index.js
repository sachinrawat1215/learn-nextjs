import Head from 'next/head';
import { useRouter } from 'next/router';
import Header from '../Components/Header';

const blog = () => {
  const router = useRouter();

  const goToBlog = () => {
    router.push('/blog');
  }

  return (
    <div style={{textAlign: "center"}}>
      <Head>
        <title>Home Page</title>
      </Head>
      <Header />
      <h1>This is our Home page...</h1>
      <button style={{padding: '10px 20px', border: 'none', backgroundColor: 'black', color: 'white', fontSize: '16px', fontWeight: '500', borderRadius: '5px', cursor: 'pointer'}} onClick={goToBlog}>View Blog</button>
    </div>
  )
}

export default blog;