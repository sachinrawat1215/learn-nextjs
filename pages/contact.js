import Header from '../Components/Header';
import Head from 'next/head';

const contact = () => {
    return (
        <div>
            <Head>
                <title>Contact Page</title>
            </Head>
            <Header />
            <h1>This is our Contact us page</h1>
        </div>
    )
}

export default contact