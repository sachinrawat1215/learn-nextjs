import Header from "../Components/Header";
import Head from "next/head";

const disclaimer = () => {
    return (
        <div>
            <Head>
                <title>Disclaimer Page</title>
            </Head>
            <Header />
            <h1>This is our Disclaimer page</h1>
        </div>
    )
}

export default disclaimer;