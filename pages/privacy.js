import Header from '../Components/Header';
import Head from 'next/head';

const privacy = () => {
    return (
        <div>
            <Head>
                <title>Privacy Page</title>
            </Head>
            <Header />
            <h1>This is our Privacy page.</h1>
        </div>
    )
}

export default privacy