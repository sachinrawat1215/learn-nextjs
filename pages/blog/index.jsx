import Header from "../../Components/Header";
import Head from "next/head";
import Link from "next/link";

export const getStaticProps = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts');
  const data = await res.json();
  // console.log(data);

  return {
    props: {
      data
    }
  }
}

// console.log({ data });

const blog = ({ data }) => {
  return (
    <div>
      <Head>
        <title>Our Blogs - All</title>
      </Head>
      <Header />
      <h1>This is main blog</h1>
      {
        data.slice(0, 10).map((element) => {
          return (<div key={element.id} className="container">
            <div className="serial">{element.id}</div>
            <Link href={`/blog/${element.id}`}><div className="content-title">{element.title}</div></Link>
          </div>);
        })
      }
    </div>
  )
}

export default blog;