import Header from '../../Components/Header';
import Head from 'next/head';
import { useRouter } from 'next/router';

export const getStaticPaths = async () => {
    const res = await fetch('https://jsonplaceholder.typicode.com/posts');
    const data = await res.json();

    const paths = data.map((currElm) => {
        return {
            params: {
                pageNo: currElm.id.toString()
            }
        }
    });

    console.log(paths);

    return ({ paths, fallback: false })
};

export const getStaticProps = async (context) => {
    // const router = useRouter();
    // console.log(router.query.pageNo)
    console.log(context);
    const id = context.params.pageNo;
    const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
    const data = await res.json();
    // console.log(data);

    return {
        props: {
            data
        }
    }
}

const pageNo = ({ data }) => {
    // console.log(data);
    return (
        <>
            <Head>
                <title>Blog No. 5</title>
            </Head>
            <Header />
            <div className="container">
                <div className="serial">{data.id}</div>
                <div>
                    <div className="content-title">{data.title}</div>
                    <div>
                        {data.body}
                    </div>
                </div>
            </div>
        </>
    )
}

export default pageNo;