import Link from "next/link";

const Header = () => {
  return (
    <div className="header">
        <ul>
            <li><Link href={'/'}>Home</Link></li>
            <li><Link href={'/about'}>About</Link></li>
            <li><Link href={'/contact'}>Contact</Link></li>
            <li><Link href={'/disclaimer'}>Disclaimer</Link></li>
            <li><Link href={'/privacy'}>Privacy</Link></li>
        </ul>
    </div>
  )
}

export default Header;